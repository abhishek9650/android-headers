Name:           android-headers
Version:        7.1
Release:        1%{?dist}
Summary:        Header files needed to write applications for the Android platform
          
License:        custom
URL: 		https://gitlab.com/abhishek9650/android-headers
Source0:        %{name}-%{version}.tar


%description
Header files needed to write applications for the Android platform.

%package -n android-24
Summary:	Header files needed to write applications for the android platform -- Hybris Headers for Android

%description -n android-24
Header files needed to write applications for the android platform -- Hybris Headers for Android.

%package -n android-24-caf
Summary:	Header files needed to write applications for the android platform -- Hybris Headers for Android

%description -n android-24-caf
Header files needed to write applications for the android platform -- Hybris Headers for Android.

%prep
%setup -q


%install -n android-24
install -m 24/android-config.h $RPM_BUILD_ROOT/%{_includedir}/android-24/android-config.h
install -m 24/android-version.h $RPM_BUILD_ROOT/%{_includedir}/android-24/android-version.h
install -m 24/android-headers-24.pc $RPM_BUILD_ROOT/%{_libexecdir}/android-24/android-headers-24.pc
install -m 24/android $RPM_BUILD_ROOT/%{_includedir}/android-24/android
install -m 24/cutils $RPM_BUILD_ROOT/%{_includedir}/android-24/cutils
install -m 24/hardware $RPM_BUILD_ROOT/%{_includedir}/android-24/hardware
install -m 24/hardware_legacy $RPM_BUILD_ROOT/%{_includedir}/android-24/hardware_legacy
install -m 24/libnfc-nxp $RPM_BUILD_ROOT/%{_includedir}/android-24/libnfc-nxp
install -m 24/system $RPM_BUILD_ROOT/%{_includedir}/android-24/system
install -m 24/linux $RPM_BUILD_ROOT/%{_includedir}/android-24/linux
install -m 24/sync $RPM_BUILD_ROOT/%{_includedir}/android-24/sync
install -m 24/private $RPM_BUILD_ROOT/%{_includedir}/android-24/private
install -m 24/log $RPM_BUILD_ROOT/%{_includedir}/android-24/log

%install -n android-24-caf
install -m 24-caf/android-config.h $RPM_BUILD_ROOT/%{_includedir}/android-24-caf/android-config.h
install -m 24-caf/android-version.h $RPM_BUILD_ROOT/%{_includedir}/android-24-caf/android-version.h
install -m 24-caf/android-headers-24-caf.pc $RPM_BUILD_ROOT/%{_libexecdir}/android-24-caf/android-headers-24-caf.pc
install -m 24-caf/android $RPM_BUILD_ROOT/%{_includedir}/android-24-caf/android
install -m 24-caf/cutils $RPM_BUILD_ROOT/%{_includedir}/android-24-caf/cutils
install -m 24-caf/hardware $RPM_BUILD_ROOT/%{_includedir}/android-24-caf/hardware
install -m 24-caf/hardware_legacy $RPM_BUILD_ROOT/%{_includedir}/android-24-caf/hardware_legacy
install -m 24-caf/libnfc-nxp $RPM_BUILD_ROOT/%{_includedir}/android-24-caf/libnfc-nxp
install -m 24-caf/system $RPM_BUILD_ROOT/%{_includedir}/android-24-caf/system
install -m 24-caf/linux $RPM_BUILD_ROOT/%{_includedir}/android-24-caf/linux
install -m 24-caf/sync $RPM_BUILD_ROOT/%{_includedir}/android-24-caf/sync
install -m 24-caf/private $RPM_BUILD_ROOT/%{_includedir}/android-24-caf/private
install -m 24-caf/log $RPM_BUILD_ROOT/%{_includedir}/android-24-caf/log


%files -n hybris-%{name}-24

%files -n hybris-%{name}-24-caf

%changelog
